#! /bin/bash

echo "Data RRBS recuperation from the repertories shared"

mkdir data

cp /home/users/shared/data/stategra/rrbs/* /home/users/student06/data

echo "Decompression of files"
gunzip /home/users/student06/data/*.gz

echo "All rrbs files"
ls /home/users/student06/data/*


echo "Reference genome Mus musculus recuperation from the repertories shared"

mkdir Refgen

cp /home/users/shared/data/Mus_musculus/current/fasta/* /home/users/student06/Refgen
