#! /bin/bash

echo 'Date: Parallels calculs Master course 2023'
echo 'Object: Sample workflow for Stategra datasets showing job execution and dependency handling.'

# Handling errorsset -x # debug mode on

set -o errexit #ensure script will stop in case of ignored error
set -o nounset #force variable initialisation
set -o verbose

### initialisation ###

echo 'conda initialisation'
module purge
module load conda/4.12.0

if conda info --envs | grep -q rrbs_env; then echo "rrbs env installed"; else conda env create -f  ../rrbs_env.yml; fi

echo 'activation of conda rrbs_env'
conda activate rrbs_env

mkdir "$HOME"/log

### Data recuperation ###

echo "$jid1 : Data and reference genome recuperation"
jid1=$(sbatch --parsable scripts/data_recuperation.sh)
echo "Data and reference genome recuperation completed"

### Pre Quality control ###

mkdir "$HOME"/quality_control
mkdir "$HOME"/quality_control/preTrimming

echo "$jid2 : Pre trimming Quality Control"
jid2=$(sbatch --parsable --dependency=afterok:$jid1 scripts/stategra_fastqc_preTrimming.slurm)
echo "Pre trimming QC completed"

### Trimming ###

mkdir "$HOME"/trimming

echo "$jid3 : Trimming with TrimGalore"
jid3=$(sbatch --parsable --dependency=afterok:$jid2 scripts/stategra_trimming.slurm)
echo "Trimming completed"

### Post Quality control ###
mkdir "$HOME"/quality_control/postTrimming

echo "$jid4 : Post trimming control_quality"
jid4=$(sbatch --parsable --dependency=afterok:$jid3 scripts/stategra_fastqc_postTrimming.slurm)
echo "Post trimming QC completed"

### Bismark Alignement preparation ###

echo "$jid5 : Alignement preparation"
jid5=$(sbatch --parsable --dependency=afterok:$jid4 scripts/stategra_bismark_preparation.slurm)
echo "Alignement prepared"

### Bismark Alignement ###

mkdir "$HOME"/results
mkdir "$HOME"/results/alignement

echo "$jid6 : Alignement"
jid6=$(sbatch --parsable --dependency=afterok:$jid5 scripts/stategra_bismark_alignement.slurm)
echo "Alignement done"


### Bismark Methylation ###

mkdir "$HOME"/results
mkdir "$HOME"/results/methylation

echo "Methylation"
jid7=$(sbatch --parsable --dependency=afterok:$jid6 scripts/stategra_bismark_methylation.slurm)
echo "Done"

# Ending step considering job efficiency
# seff