# Explication of the project   by Ankerl Yassine and Defontis Cécilia

This project is part of the Parallel Computing and GPU Programming course in UCA's M2 bioinformatics program.

This Git repository is used to process RRBS data from the publication of Green, D., Tarazona, S., Ferreirós-Vidal, I., Ramírez, R. N., Schmidt, A., Reijmers, T. H., Von Saint Paul, V., Marabita, F., Rodríguez-Ubreva, J., García-Gómez, A., Carroll, T., Cooper, L., Liang, Z., Dharmalingam, G., Van Der Kloet, F., Harms, A. C., Balzano‐Nogueira, L., Lagani, V., Tsamardinos, I.,. . . Conesa, A. (2019b). STATEGra, a comprehensive multi-omics dataset of B-cell differentiation in mouse. Scientific Data, 6(1). https://doi.org/10.1038/s41597-019-0202-7 .
https://www.nature.com/articles/s41597-019-0202-7

This work was carried out on an HPC2 cluster and the scripts used are interpreted by Slurm.
The following workflow is ment to be used on the UCA Mesocentre HPC cluster

# Tools you will need
conda - fastqc - trimGalore! - bismark - bowtie2 - R - python

Don't need a specific version for each tools

All the tools are available on Bioconda, but here we already gave you the conda environnement containing all the Tools

# Description of your repertories
`
```
.
├── READMEslurm.md
├── rrbs_env.yml
└── scripts
    ├── data_recuperation.sh
    ├── stategra_allScripts.sh
    ├── stategra_bismark_alignement.slurm
    ├── stategra_bismark_methylation.slurm
    ├── stategra_bismark_preparation.slurm
    ├── stategra_fastqc_postTrimming.slurm
    ├── stategra_fastqc_preTrimming.slurm
    └── stategra_trimGalore.slurm
```

`
The scripts will create others repertories like :

results               for your results of alignement and methylation

log                   to keep all the log

quality_control       for your results of quality control pre and post trimming

trimming              for your trimmed data

# Environnement conda and scripts

To start the pipeline clone this repository HTTPS in your $HOME on linux terminal
`git clone https://gitlab.com/cedefontis/uca_m2bi_hpc.git `
And use the command `sbatch scripts/stategra_allScripts.sh`

Module and conda environnement will be automaticaly installed if abscent and activated through the script

Pre Quality control with FastQC
with the script scripts/stategra_fastqc_preTrimming.slurm

Trimming with TrimGalore
With the script scripts/stategra_trimming.slurm

Post Quality control with FastQC
With the script scripts/stategra_fastqc_postTrimming.slurm

Bismark preparation
with the script scripts/stategra_bismark_preparation.slurm

Bismark alignement 
With the script scripts/stategra_bismark_alignement.slurm

Methylation analyse
With the script scripts/stategra_bismark_methylation.slurm
