# Explication of the project Slurm and Snakemake   by Defontis Cécilia 📖

This projects is part of the Parallel Computing and GPU Programming course in UCA's M2 bioinformatics program.

This Git repository is used to process RRBS data from the publication of Green, D., Tarazona, S., Ferreirós-Vidal, I., Ramírez, R. N., Schmidt, A., Reijmers, T. H., Von Saint Paul, V., Marabita, F., Rodríguez-Ubreva, J., García-Gómez, A., Carroll, T., Cooper, L., Liang, Z., Dharmalingam, G., Van Der Kloet, F., Harms, A. C., Balzano‐Nogueira, L., Lagani, V., Tsamardinos, I.,. . . Conesa, A. (2019b). STATEGra, a comprehensive multi-omics dataset of B-cell differentiation in mouse. Scientific Data, 6(1). https://doi.org/10.1038/s41597-019-0202-7 .
https://www.nature.com/articles/s41597-019-0202-7

This work was carried out on an HPC2 cluster and the scripts used are interpreted by Slurm or Snakemake scripts
The following workflow is ment to be used on the UCA Mesocentre HPC cluster

# Modules and Tools you will need ​🔧​
gcc/8.1.0 conda/4.12.0 bowtie2/2.3.4.3 python/3.7.1 snakemake/7.15.1

fastqc (https://anaconda.org/bioconda/fastqc) trimGalore! (https://anaconda.org/bioconda/trim-galore) bismark (https://anaconda.org/bioconda/bismark)

All the tools for the analysis of the data are available on Bioconda, but for each project I gave you the conda environnement containing all the Tools in de directory envs
Module and conda environnement will be automaticaly installed with the script after you git clone the git

`git clone https://gitlab.com/cedefontis/uca_m2bi_hpc.git`

You will have the 2 repertories for slurm's scripts and snakemake's scripts 
Please be on the good repertories before run scripts depending on whether if you want to do slurm or snakemake

NB : the snakemake's project is more structured and qualitative than the slurm's project
# Structure ​​🗂️​
The structure of the global git

```
.
├── projetSlurm
│   ├── READMEslurm.md
│   ├── rrbs_env.yml
│   └── scripts
│       ├── data_recuperation.sh
│       ├── stategra_allScripts.sh
│       ├── stategra_bismark_alignement.slurm
│       ├── stategra_bismark_methylation.slurm
│       ├── stategra_bismark_preparation.slurm
│       ├── stategra_fastqc_postTrimming.slurm
│       ├── stategra_fastqc_preTrimming.slurm
│       └── stategra_trimGalore.slurm
├── projetSnakemake
│   ├── config
│   │   └── AllDataConfig.yaml
│   ├── data
│   │   └── rrbs -> /home/users/shared/data/stategra/rrbs/
│   ├── envs
│   │   └── rrbs_env.yml
│   ├── READMEsnakemake.md
│   ├── refgen
│   │   └── fasta -> /home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/fasta/
│   └── scripts
│       ├── alignement.smk
│       ├── methylation_analysis.smk
│       ├── QualityControlPostTrimming.smk
│       ├── QualityControlPreTrimming.smk
│       ├── stategra_allScriptsSnakemake.sh
│       └── trimming.smk
└── README.md

```

# Execution ​​​📝​

for slurm`bash projetSlurm/scripts/stategra_allScripts.sh`

for snakemake`bash projetSnakemake/scripts/stategra_allScriptsSnakemake.sh`

Pre Quality control with FastQC

Trimming with TrimGalore

Post Quality control with FastQC

Bismark alignement 

Methylation analyse
