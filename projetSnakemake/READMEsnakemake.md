# Explication of the project   by Defontis Cécilia 📖

This project is part of the Parallel Computing and GPU Programming course in UCA's M2 bioinformatics program.

This Git repository is used to process RRBS data from the publication of Green, D., Tarazona, S., Ferreirós-Vidal, I., Ramírez, R. N., Schmidt, A., Reijmers, T. H., Von Saint Paul, V., Marabita, F., Rodríguez-Ubreva, J., García-Gómez, A., Carroll, T., Cooper, L., Liang, Z., Dharmalingam, G., Van Der Kloet, F., Harms, A. C., Balzano‐Nogueira, L., Lagani, V., Tsamardinos, I.,. . . Conesa, A. (2019b). STATEGra, a comprehensive multi-omics dataset of B-cell differentiation in mouse. Scientific Data, 6(1). https://doi.org/10.1038/s41597-019-0202-7 .
https://www.nature.com/articles/s41597-019-0202-7

This work was carried out on an HPC2 cluster and the scripts used are interpreted by Snakemake
The following workflow is ment to be used on the UCA Mesocentre HPC cluster

# Modules and Tools you will need ​🔧​
gcc/8.1.0 conda/4.12.0 bowtie2/2.3.4.3 python/3.7.1 snakemake/7.15.1

fastqc (https://anaconda.org/bioconda/fastqc) trimGalore! (https://anaconda.org/bioconda/trim-galore) bismark (https://anaconda.org/bioconda/bismark)

All the tools for the analysis of the data are available on Bioconda, but here we already gave you the conda environnement containing all the Tools in de directory envs
Module and conda environnement will be automaticaly installed with the script after you git clone the projetSnakemake

`git clone https://gitlab.com/cedefontis/uca_m2bi_hpc.git`

# Structure ​​🗂️​
The structure that the snakemake pipeline will generate once the analysis has been completed  

```
.
├── **config**
│   └── AllDataConfig.yaml
├── **data**
│   └── rrbs -> /home/users/shared/data/stategra/rrbs/
├── **envs**
│   └── rrbs_env.yml
├── READMEsnakemake.md
├── **refgen**
│   └── fasta -> /home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/fasta/
├── **quality_control**
│   └── preTrimming
│   └── postTrimming
├── **trimming**
├── **results**
│   └── alignement
│   └── methylation
└── **scripts**
    ├── alignement.smk
    ├── methylation_analysis.smk
    ├── QualityControlPostTrimming.smk
    ├── QualityControlPreTrimming.smk
    ├── stategra_allScriptsSnakemake.sh
    └── trimming.smk
```

data -- to have access to the raw rrbs data

refgen -- to have the Mus musculus genome

results -- for your results of alignement and methylation

log -- to keep all the log

quality_control -- for your results of quality control pre and post trimming

trimming  -- for your trimmed data

script  -- to put and call your scripts

# Execution ​​​📝​
`bash stategra_allScriptsSnakemake.sh`

Pre Quality control with FastQC
with the script scripts/QualityControlPreTrimming.smk

Trimming with TrimGalore
With the script scripts/trimming.smk

Post Quality control with FastQC
With the script scripts/QualityControlPostTrimming.smk

Bismark alignement 
With the script scripts/alignement.smk

Methylation analyse
With the script scripts/methylation_analysis.smk
