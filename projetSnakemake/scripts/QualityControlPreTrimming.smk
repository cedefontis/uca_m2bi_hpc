### Quality control pre trimming with the tool fastQC ###

configfile : "config/AllDataConfig.yaml"

rule QualityControlePreTrimming:
    input: 
        expand("data/rrbs/{sample}_1.fastq.gz", sample=config["samples"]),
        expand("data/rrbs/{sample}_2.fastq.gz", sample=config["samples"])
    output:
        expand("quality_control/preTrimming/{sample}_1.fastqc.html" , sample=config["samples"]),
        expand("quality_control/preTrimming/{sample}_1.fastqc.zip" , sample=config["samples"]),
        expand("quality_control/preTrimming/{sample}_2.fastqc.html" , sample=config["samples"]),
        expand("quality_control/preTrimming/{sample}_2.fastqc.zip" , sample=config["samples"])
    conda : 
        "../envs/rrbs_env.yml"
    resources:
        mem_mb=5,
        time="00:10:00"
    threads: 4
    shell:
        """
        fastqc -o quality_control/preTrimming/ -t {threads} {input}
        """