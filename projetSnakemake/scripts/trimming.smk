### Trimming with the tool trimGalore! ###

configfile : "config/AllDataConfig.yaml"

rule all:
    input:
        expand("trimming/{sample}_1.fastq.gz_trimming_report.txt", sample=config["samples"]),
        expand("trimming/{sample}_1_val_1.fq.gz", sample=config["samples"]),
        expand("trimming/{sample}_2.fastq.gz_trimming_report.txt", sample=config["samples"]),
        expand("trimming/{sample}_2_val_2.fq.gz", sample=config["samples"])

rule trimming:
    input:
        forward_file="data/rrbs/{sample}_1.fastq.gz",
        reverse_file="data/rrbs/{sample}_2.fastq.gz"
    output:
        "trimming/{sample}_1.fastq.gz_trimming_report.txt",
        "trimming/{sample}_1_val_1.fq.gz",
        "trimming/{sample}_2.fastq.gz_trimming_report.txt",
        "trimming/{sample}_2_val_2.fq.gz"
    conda: 
        "../envs/rrbs_env.yml"
    resources:
        mem_mb=3000,
        time="01:00:00"
    threads: 8
    shell:
        """
        trim_galore --rrbs --paired -q 30 -j {threads} --clip_R1 5 --clip_R2 5 --three_prime_clip_R1 3 --three_prime_clip_R2 3  \
        {input.forward_file} {input.reverse_file} -o trimming/
        """