### Methyalation analysis with the tool bismark ###

configfile : "config/AllDataConfig.yaml"

rule all:
    input:
        expand("../results/methylation/{sample}", sample=config["samples_compiled"])

rule methylation:
    input:
        expand("../results/alignement/{sample}_1_val_1_bismark_bt2_pe.bam", sample=config["samples_compiled"]),
        expand("../results/alignement/{sample}_2_val_2_bismark_bt2_pe.bam", sample=config["samples_compiled"])  
    output:
        "../results/methylation/{sample}"
    params:
        index = "refgen/"
    threads: 12
    conda : 
        "../envs/rrbs_env.yml"
    resources:
        mem_mb=4000,  
        time="01:00:00"
    shell:
        """
    bismark_methylation_extractor -p --bedGraph --counts {input} -o results/methylation/
        """