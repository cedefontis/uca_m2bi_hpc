#! /bin/bash

echo 'Date: Parallels calculs Master course 2023-2024'
echo 'Object: Sample workflow for Stategra datasets with Snakemake.'

echo 'Loading modules'
module purge
module load gcc/8.1.0 python/3.7.1 
module load snakemake/7.15.1
module load bowtie2/2.3.4.3

### Environnement and Data initialisation ###

echo 'Repertories creation'
mkdir data refgen log quality_control quality_control/preTrimming quality_control/postTrimming trimming results results/alignement results/methylation
ln -s /home/users/shared/data/stategra/rrbs/ ../data/rrbs
cp /home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/fasta/* refgen/

### Pre Quality control ###

echo "Start Pre trimming Quality Control"
snakemake --cores 24 --jobs 24 --snakefile scripts/QualityControlPreTrimming.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
echo "Pre trimming QC completed"

### Trimming ###

echo "Start Trimming with TrimGalore"
snakemake --cores 24 --jobs 24 --snakefile scripts/trimming.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
echo "Trimming completed"

### Post Quality control ###

echo "Start Post trimming control_quality"
snakemake --cores 24 --jobs 24 --snakefile scripts/QualityControlPostTrimming.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
echo "Post trimming QC completed"

### Bismark Alignement preparation ###

echo "Start Alignement preparation"
conda activate rrbs_env
bismark_genome_preparation /refgen/fasta
conda deactivate
echo "Alignement preparation completed"

### Bismark Alignement ###

echo "Start Alignement"
snakemake --cores 24 --jobs 24 --snakefile scripts/alignement.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
echo "Alignement completed"

### Bismark Methylation ###

echo "Start Methylation"
snakemake --cores 24 --jobs 24 --snakefile scripts/methylation_analysis.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
echo "Methylation analysis completed"

### End ###