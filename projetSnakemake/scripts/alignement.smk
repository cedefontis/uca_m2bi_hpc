### Alignement with the tool bismark ###

configfile : "config/AllDataConfig.yaml"

rule all:
    input:
        expand("results/alignement/{sample}_1_val_1_bismark_bt2_PE_report.txt", sample=config["samples"]),
        expand("results/alignement/{sample}_2_val_2_bismark_bt2_PE_report.txt", sample=config["samples"]),
        expand("results/alignement/{sample}_1_val_1_bismark_bt2_pe.bam", sample=config["samples"]),
        expand("results/alignement/{sample}_2_val_2_bismark_bt2_pe.bam", sample=config["samples"])

rule alignement:
    input:
        file1 = "trimming/{sample}_1_val_1.fq.gz",
        file2 = "trimming/{sample}_2_val_2.fq.gz"
    output:
        "results/alignement/{sample}_1_val_1_bismark_bt2_PE_report.txt",
        "results/alignement/{sample}_2_val_2_bismark_bt2_PE_report.txt",
        "results/alignement/{sample}_1_val_1_bismark_bt2_pe.bam",
        "results/alignement/{sample}_2_val_2_bismark_bt2_pe.bam"    
    params:
        index = "refgen/"
    threads: 12
    conda : 
        "../envs/rrbs_env.yml"
    resources:
        mem_mb=4000,  
        time="02:00:00"
    shell:
        """
        bismark {params.index} -p {threads} -1 {input.file1} -2 {input.file2} -o results/alignement/
        """