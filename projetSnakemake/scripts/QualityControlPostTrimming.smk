### Quality control post trimming with the tool fastQC ###

configfile : "config/AllDataConfig.yaml"

rule QualityControlePostTrimming:
    input: 
        expand("trimming/{sample}_1_val_1.fq.gz", sample=config["samples"]),
        expand("trimming/{sample}_2_val_2.fq.gz", sample=config["samples"])
    output:
        expand("quality_control/postTrimming/{sample}_1_val_1_fastqc.html" , sample=config["samples"]),
        expand("quality_control/postTrimming/{sample}_1_val_1_fastqc.zip" , sample=config["samples"]),
        expand("quality_control/postTrimming/{sample}_2_val_2_fastqc.html" , sample=config["samples"]),
        expand("quality_control/postTrimming/{sample}_2_val_2_fastqc.zip" , sample=config["samples"])
    conda : 
        "../envs/rrbs_env.yml"
    resources:
        mem_mb=5,
        time="00:10:00"
    threads: 4
    shell:
        """
        fastqc -o quality_control/postTrimming/ -t {threads} {input}
        """